import {
  ApolloGateway,
  RemoteGraphQLDataSource,
  IntrospectAndCompose,
} from "@apollo/gateway";
import { ApolloServer } from "apollo-server";
import { ApolloServerPluginLandingPageGraphQLPlayground } from "apollo-server-core";

class AuthenticatedDataSource extends RemoteGraphQLDataSource {
  willSendRequest({ request, context }) {
    request.http.headers.set("cookie", context.cookie);
  }
  didReceiveResponse({ response, context }) {
    const responseHeder = response.http.headers;
    if (responseHeder.has("set-cookie")) {
      const cookie = responseHeder.get("set-cookie");
      const setCookies = cookie.split(",") as string[];
      context.res.setHeader("set-cookie", setCookies);
    }
    return response;
  }
}

const gateway = new ApolloGateway({
  supergraphSdl: new IntrospectAndCompose({
    subgraphs: [
      {
        name: "general",
        url: process.env.GENERAL_ENDPOINT || "http://localhost:5000/graphql",
      },
      {
        name: "nlp",
        url: process.env.NLP_ENDPOINT || "http://localhost:5001/graphql",
      },
    ],
  }),
  buildService: ({ url }) => {
    return new AuthenticatedDataSource({ url });
  },
});

const myPlugin = {
  // Fires whenever a GraphQL request is received from a client.
  async requestDidStart(requestContext) {
    console.log("Request started! Query");
    console.log(
      "from orgin: ",
      requestContext.request.http.headers.get("origin")
    );

    return {
      async serverWillStart() {
        console.log("Server starting up!");
      },
      // Fires whenever Apollo Server will parse a GraphQL
      // request to create its associated document AST.
      async parsingDidStart(requestContext) {
        console.log("Parsing started!");
      },

      // Fires whenever Apollo Server will validate a
      // request's document AST against your GraphQL schema.
      async validationDidStart(requestContext) {
        console.log("Validation started!");
      },
    };
  },
};

const server = new ApolloServer({
  gateway,
  introspection: true,
  plugins: [
    myPlugin,
    ApolloServerPluginLandingPageGraphQLPlayground({
      settings: {
        "request.credentials": "include",
      },
    }),
  ],
  cors: {
    origin: new RegExp(process.env.ORIGIN || ".*"),
    credentials: true,
  },
  context: (ctx) => {
    const { req, res } = ctx;
    return {
      req,
      res,
      cookie: ctx.req.headers.cookie,
    };
  },
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
